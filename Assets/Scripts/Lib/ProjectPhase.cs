﻿namespace Commanders.Assets.Scripts.Lib {
    internal enum ProjectPhase {
        Release = 0,
        Testing = 1,
        Development = 2
    }
}
