﻿/// —————————————————————————————————————————————
//? 
//!? 📜 DimensionType.cs
//!? 🖋️ Galacticai 📅 2022
//!  ⚖️ GPL-3.0-or-later
//?  🔗 Dependencies: No special dependencies
//? 
/// —————————————————————————————————————————————

namespace Commanders.Assets.Scripts.Lib.Math.Space2D {
    public enum DimensionType {
        time, x, y
    }
}
