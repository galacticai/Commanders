﻿/// —————————————————————————————————————————————
//? 
//!? 📜 DimensionType.cs
//!? 🖋️ Galacticai 📅 2022
//!  ⚖️ GPL-3.0-or-later
//?  🔗 Dependencies: No special dependencies
//? 
/// —————————————————————————————————————————————

namespace Commanders.Assets.Scripts.Lib.Math.Space3D {
    public enum DimensionType {
        time, x, y, z
    }
}
