﻿﻿<h1 align="center">Commanders</h1>

<h4 align="center"><b>Open-Source Real-Time Strategy Game</b></h4>

<hr/>
<br/>

<h5 align="center">
    <b>🟨⬛🟨⬛🟨⬛🟨⬛🟨⬛🟨</b> 
    <br/><br/>
    <img src="https://img.shields.io/badge/-UNDER%20CONSTRUCTION-yellow?style=for-the-badge" />
    <br/>
    <img src="https://img.shields.io/badge/-Yes,%20working%20on%20it,%20even%20if%20no%20commits-gray?style=flat-square" /> 
    <br/><br/>
    <b>🟨⬛🟨⬛🟨⬛🟨⬛🟨⬛🟨</b>
</h5>

<br/>

> <img src="https://img.shields.io/badge/-Targeting-gray" />
> <img src="https://img.shields.io/badge/-Windows-white?logo=windows-11&logoColor=0078D4" />
> <img src="https://img.shields.io/badge/-Linux-white?logo=linux&logoColor=806412" />
> <br/>
> <img src="https://img.shields.io/badge/-Made%20with-gray" />
> <img src="https://img.shields.io/badge/-Unity%20Engine-white?logo=unity&logoColor=black" />
> <img src="https://img.shields.io/badge/-C%23-white?logo=dotnet&logoColor=512BD4" />
> <br/>
> <img src="https://img.shields.io/badge/%C2%A92022-Galacticai-white?link=https://github.com/Galacticai" />
> <img src="https://img.shields.io/github/license/Galacticai/Commanders?label=&color=white&logo=gnu&logoColor=A42E2B">
> <br/>
> <br/>
> <a href="https://www.codacy.com/gh/Galacticai/Commanders/dashboard" >
>     <img src="https://img.shields.io/codacy/grade/5c46c2f9d1aa42118fd39fca9a3ce381?logo=codacy&label=Codacy%20rating">
> </a>
> <br/>
> <a href="https://github.com/Galacticai/Commanders/contributors">
>     <img src="https://img.shields.io/github/contributors/Galacticai/Commanders?label=Contributors&logo=git&logoColor=white" />
> </a>
> <br/>
> <a href="https://github.com/Galacticai/Commanders/commits">
>     <img src="https://img.shields.io/badge/-Commits-4F4F4F?logo=git&logoColor=white" />
>     <img src="https://img.shields.io/github/last-commit/Galacticai/Commanders?label=&color=white" />
>     <img src="https://img.shields.io/github/commit-activity/m/Galacticai/Commanders?label=&color=white" />
> <br/>
> <a href="https://github.com/Galacticai/Commanders/issues">
>     <img src="https://img.shields.io/badge/-Issues-4F4F4F?logo=github" />
>     <img src="https://img.shields.io/github/issues/Galacticai/Commanders?label=&color=white" />
>     <img src="https://img.shields.io/github/issues-closed/Galacticai/Commanders?label=&color=white" />
> </a>
> </a>

<br/>

<hr/>

<h2 align=center>🌟 Highlights </h2>

<img src="https://picsum.photos/1024/128"/>

### 🎨 Highlight 1
Lorem Ipsum is simply dummy text  Lorem Ipsum has been the industry.

<br/>

<img src="https://picsum.photos/1024/128"/>

### 🖼 Highlight 2
Lorem Ipsum is simply dummy text  Lorem Ipsum has been the industry.

<br/>

<img src="https://picsum.photos/1024/128"/>

### 👑 Highlight 3
Lorem Ipsum is simply dummy text  Lorem Ipsum has been the industry.

<br/>

<img src="https://picsum.photos/1024/128"/>

### 🌏 Highlight 4
Lorem Ipsum is simply dummy text  Lorem Ipsum has been the industry.

<br/>

<hr/>

<h2 align=center> 🚀 Install </h2>

### 🛡 <u>Official installer</u>
Lorem Ipsum is simply dummy text  Lorem Ipsum has been the industry's, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting.

<br/>

### 🏗 Build from sources
Lorem Ipsum is simply dummy text  Lorem Ipsum has been the industry's, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting.

<br/>

<hr/>

<h2 align=center> 🦾 How to play </h2>

<img src="https://picsum.photos/1024/256"/>

> ### Interesting title 1
> Lorem Ipsum is simply dummy text  Lorem Ipsum has been the industry's, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting.

<br/>

<img src="https://picsum.photos/1024/256"/>

> ### Interesting title 2
> Lorem Ipsum is simply dummy text  Lorem Ipsum has been the industry's, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting.

<br/>

<img src="https://picsum.photos/1024/256"/>

> ### Interesting title 3
> Lorem Ipsum is simply dummy text  Lorem Ipsum has been the industry's, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting.

<br/>

<hr/>

<h2 align=center> ❌ Uninstall </h2>

<p>
    <img height=96 src="https://i.pinimg.com/236x/7d/02/c4/7d02c4d3f92755c0e0ebcc75edbb252a--sad-kitty-sad-cat.jpg"/>
    <img height=96 src="https://i.pinimg.com/originals/5f/96/2f/5f962fb5b03c7bea7ab526199b983d60.jpg"/>
    <img height=96 src="https://i.pinimg.com/originals/be/54/62/be5462eb289bdd698c9fc328406eed07.jpg"/>
    <img height=96 src="https://images.freeimages.com/images/large-previews/b1a/the-sad-kitten-1484446.jpg" />
</p>

> WAIT Look at these cats... They are probably sad because you are reading this section...

<br/>

Lorem Ipsum is simply dummy text  Lorem Ipsum has been the industry's, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting.

<br/>

<br/>
